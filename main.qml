import QtQuick 2.8
import QtQuick.Controls 2.1

import "./"

ApplicationWindow {
    id: window

    visible: true
    width: 640
    height: 480
    title: qsTr("ContextMenu")

    header: ToolBar {
        ToolButton {
            id: menuButton

            anchors.right: parent.right
            enabled: !contextMenu.visible

            contentItem: Image {
                fillMode: Image.Pad
                horizontalAlignment: Image.AlignHCenter
                verticalAlignment: Image.AlignVCenter
                source: "qrc:/images/menu.png"
            }

            onClicked: {
                contextMenu.open()
            }

            ContextMenu {
                id: contextMenu
            }
        } // ToolButton { id: menuButton
    } // header: ToolBar {

    // How to get radio button value
    Column {
        anchors.centerIn: parent
        spacing: 10

        Label {
            text: contextMenu.secondItem.text + ": " + contextMenu.secondItem.radionButtonText + ", id: " + contextMenu.secondItem.radionButtonIndex
        }

        Label {
            text: contextMenu.fourItem.text + ": " + contextMenu.fourItem.radionButtonText + ", id: " + contextMenu.fourItem.radionButtonIndex
        }
    } // Column {
}
