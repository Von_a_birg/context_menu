import QtQuick 2.8
import QtQuick.Controls 2.1

import "./"

Menu {
    id: _contextMenu

    readonly property alias secondItem: secondItem
    readonly property alias fourItem  : fourItem

    signal closeAllSubmenu()

    topMargin: menuButton.contentItem.height
    rightMargin: Consts.menuMagrin
    topPadding: Consts.menuMagrin
    bottomPadding: Consts.menuMagrin
    clip: true

    background: Rectangle {
        implicitWidth: Consts.menuWidth
        border.color: "gray"
        radius: Consts.radius
    }

    //--------------------------------------------------------------------------
    // ContextMenu items
    //--------------------------------------------------------------------------
    ContextMenuItem { text: qsTr("Первый") }

    ContextMenuItem {
        id: secondItem

        text: qsTr("Второй")
        radioButtonsGroup: [qsTr("Альфа"), qsTr("Бета")]
    }

    ContextMenuItem {  text: qsTr("Третий") }

    MenuSeparator {
        contentItem: Rectangle {
            width: parent.width
            implicitHeight: 1
            color: Consts.separatorColor
        }
    }

    ContextMenuItem {
        id: fourItem

        text: qsTr("Четвертый")
        radioButtonsGroup: [qsTr("Эпсилон"), qsTr("Лямбда")]
    }

    ContextMenuItem { text: qsTr("Пятый") }

    //--------------------------------------------------------------------------
    // Animation
    //--------------------------------------------------------------------------
    enter: Transition {
        NumberAnimation { property: "scale";   from: 0.9; to: 1.0; easing.type: Easing.OutCubic; duration: 200 }
        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0; easing.type: Easing.OutCubic; duration: 150 }
    }

    exit: Transition {
        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0; duration: 100 }
    }
} // Menu {

