import QtQuick 2.8
import QtQml 2.2
import QtQuick.Controls 2.1

import "./"

MenuItem {
    id: menuElement

    property var radioButtonsGroup: ([])
    property string radionButtonText: ""
    property int radionButtonIndex: 0

    highlighted: hovered
    hoverEnabled: true
    font.pixelSize: Consts.fontNormal

    background: Rectangle {
        width: parent.width - 2*_contextMenu.background.border.width
        anchors.horizontalCenter: parent.horizontalCenter
        visible: parent.highlighted
        color: Consts.higlightColor
    }

    onHoveredChanged: {
        hoveredHook(hovered);
    }

    //--------------------------------------------------------------------------
    // Additional submenu including radioButtons
    //--------------------------------------------------------------------------
    Menu {
        id: contextSubmenu

        x: -width
        background: Rectangle {
            implicitWidth: Consts.menuWidth
            border.color: "gray"
            radius: Consts.radius
        }

        //--------------------------------------------------------------------------
        // Dynamic radioButtons creator from _submenuModel
        //--------------------------------------------------------------------------
        Instantiator {
            model: radioButtonsGroup

            onObjectAdded: contextSubmenu.insertItem( index, object )
            onObjectRemoved: contextSubmenu.removeItem( object )
            delegate: RadioDelegate {
                id: radioButton

                onCheckedChanged: {
                    if (checked) {
                        radionButtonText = text
                        radionButtonIndex = index
                    }
                }

                text: radioButtonsGroup[index]
                font.pixelSize: Consts.fontNormal
                leftPadding: Consts.radioIndicatorHeight + 10

                indicator: Rectangle {
                    implicitWidth: Consts.radioIndicatorHeight
                    implicitHeight: Consts.radioIndicatorHeight
                    anchors {
                        left: parent.left
                        leftMargin: 5
                        verticalCenter: parent.verticalCenter
                    }

                    radius: 0.5*implicitWidth
                    border.color: "grey"

                    Rectangle {
                        width: Consts.radioIndicatorCheckHeight
                        height: Consts.radioIndicatorCheckHeight
                        radius: 0.5*width
                        anchors.centerIn: parent
                        color: "grey"
                        visible: radioButton.checked
                    } // Rectangle {
                } // indicator: Rectangle {

                Component.onCompleted: {
                    checked = index === 0
                }
            } // delegate: RadioDelegate {
        } // Instantiator {
    } // Menu { id: contextSubmenu

    //--------------------------------------------------------------------------
    Connections {
        target: _contextMenu
        onCloseAllSubmenu: {
            closeSubmenu();
        }
    }

    //--------------------------------------------------------------------------
    function hoveredHook(hovered) {
        if (hovered) {
            _contextMenu.closeAllSubmenu()
            if (radioButtonsGroup.length > 0) {
                contextSubmenu.open()
                menuElement.highlighted = true
            }
        }
    } // function hoveredHook(hovered) {

    //--------------------------------------------------------------------------
    function closeSubmenu() {
        if (radioButtonsGroup.length > 0) {
            contextSubmenu.close()
            menuElement.highlighted = false
        }
    } // function closeSubmenu() {
}
