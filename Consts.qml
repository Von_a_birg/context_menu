pragma Singleton
import QtQuick 2.8

QtObject {
    readonly property int radius: 5

    //Fonts
    readonly property int fontNormal: 12

    //ContextMenu
    readonly property int menuMagrin: 5
    readonly property int menuWidth : 120
    readonly property color separatorColor: "#1E000000"
    readonly property color higlightColor: "#eeeeee"

    //RadioButtton
    readonly property int radioIndicatorHeight: 16
    readonly property int radioIndicatorCheckHeight: 10
}
